import { Component, Input, OnInit } from '@angular/core';
import { PersonalityTest, Question } from 'src/app/models/PersonalityTest';
import { ApiCallsService } from 'src/app/services/ApiCalls.service';

@Component({
  selector: 'app-radar-chart',
  templateUrl: './radar-chart.component.html',
  styleUrls: ['./radar-chart.component.css']
})
export class RadarChartComponent implements OnInit {
  // @Input() personalityTest: PersonalityTest;
  personalityTest: PersonalityTest;
  data = {}
  //Set Chart

  constructor(private apiCallsService: ApiCallsService) {
    this.personalityTest = this.apiCallsService.getThePersonalityTestDone();

    this.data = {
      labels: [],
      datasets: [
        {
          label: '100 = Extrovert / 25 = Introvert',
          data: [],
          borderColor: "#3f51b5",
          backgroundColor: "rgba(255,255,255,0.1)",
        }
      ]
    }

    this.personalityTest.Questions.forEach((question: Question, index) => {
      this.data["labels"].push(index + 1);
      this.data["datasets"][0]["data"].push(question.Selected * 25);
    });

  }

  ngOnInit(): void {
    console.log(this.personalityTest);
  }

}
