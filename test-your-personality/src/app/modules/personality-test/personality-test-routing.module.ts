import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonalityTestComponent } from './personality-test.component';

const routes: Routes = [
  {
    path: "",
    component: PersonalityTestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalityTestRoutingModule { }
