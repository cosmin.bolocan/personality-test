import { PersonalityTestRoutingModule } from './personality-test-routing.module';
import { PersonalityTestComponent } from './personality-test.component';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio'
import { MatMenuModule } from '@angular/material/menu';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RadarChartComponent } from 'src/app/modules/personality-test/radar-chart/radar-chart.component';
import { ChartModule } from 'primeng/chart';

@NgModule({
  declarations: [
    PersonalityTestComponent,
    RadarChartComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    PersonalityTestRoutingModule,
    FlexLayoutModule,
    ChartModule,

    //Material imports
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatStepperModule,
    MatProgressBarModule,
    MatRadioModule,
    MatProgressSpinnerModule,

  ]
})
export class PersonalityTestModule { }
