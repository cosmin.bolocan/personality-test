import { Component, OnInit } from '@angular/core';
import { PersonalityTest, Question } from 'src/app/models/PersonalityTest';
import { ApiCallsService } from 'src/app/services/ApiCalls.service';

@Component({
  selector: 'app-personality-test',
  templateUrl: './personality-test.component.html',
  styleUrls: ['./personality-test.component.scss']
})
export class PersonalityTestComponent implements OnInit {
  personalityTest: PersonalityTest = null;
  currentQuestion: number = 0;
  progressBarValue: number = 0;
  progressBarStep: number = 0;
  testHasEnd = false;
  personalitytestResult: string = "";
  personalitytestResultDescription: string = "";

  constructor(
    public apiCallsService: ApiCallsService
  ) { }

  ngOnInit() {
    this.initializeTest();
  }

  initializeTest(){
    this.apiCallsService.getPersonalityTest().subscribe((response: PersonalityTest) => {
      this.personalityTest = response;

      this.personalityTest.Questions.forEach((question: Question) => {
        question.Selected = 0;
      });

      this.progressBarValue = Math.floor(100 / this.personalityTest.Questions.length);
      this.progressBarStep = Math.floor(100 / this.personalityTest.Questions.length);
    })
  }

  navigateToQuestion(nextQuestion: number) {
    if (nextQuestion == -1) {
      this.testHasEnd = true;
      this.progressBarValue = 100;

      this.calculateResult();
    } else {
      if (nextQuestion == this.personalityTest.Questions.length - 1) {
        this.progressBarValue = 100;
      } else {
        this.progressBarValue = (nextQuestion + 1) * this.progressBarStep;
      }
      this.currentQuestion = nextQuestion;
    }
  }

  calculateResult() {

    let answeredQuestionsCounter = 0;
    let answeredQuestionsScore = 0;

    this.personalityTest.Questions.forEach((question: Question) => {
      if (question.Selected > 0) {
        answeredQuestionsCounter++;
        answeredQuestionsScore += question.Selected;
      }

      if (answeredQuestionsScore / answeredQuestionsCounter < 2.5) {
        this.personalitytestResult = "Introvert";
        this.personalitytestResultDescription = `An introvert is often thought of as a quiet, reserved, and thoughtful individual. They don't seek out special attention or social engagements, as these events can leave introverts feeling exhausted and drained. ... They aren't one to miss a social gathering, and they thrive in the frenzy of a busy environment.`;
      } else {
        this.personalitytestResult = "Extrovert";
        this.personalitytestResultDescription = `On the positive side, extroverts are often described as talkative, sociable, action-oriented, enthusiastic, friendly, and out-going. On the negative side, they are sometimes described as attention-seeking, easily distracted, and unable to spend time alone.`;
      }
    });

    this.apiCallsService.setThePersonalityTestDone(this.personalityTest);
  }

  restartTest(){
    this.initializeTest();
    this.testHasEnd = false;
    this.currentQuestion = 0;
  }
}
