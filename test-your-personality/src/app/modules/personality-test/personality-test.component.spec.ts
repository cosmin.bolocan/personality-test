/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { PersonalityTest } from 'src/app/models/PersonalityTest';
import { ApiCallsService } from 'src/app/services/ApiCalls.service';
import { PersonalityTestComponent } from './personality-test.component';


describe('PersonalityTestComponent', () => {
  let component: PersonalityTestComponent;
  let fixture: ComponentFixture<PersonalityTestComponent>;
  let apiCallsService: ApiCallsService;
  let personalityTest: PersonalityTest = {
    Questions: [
      {
        Description: "Test Question 1",
        Answers: [
          {
            Description: "Answer 1",
            Grade: 1
          },
          {
            Description: "Answer 2",
            Grade: 2
          },
          {
            Description: "Answer 3",
            Grade: 3
          },
        ]
      },
      {
        Description: "Test Question 2",
        Answers: [
          {
            Description: "Answer 1",
            Grade: 1
          },
          {
            Description: "Answer 2",
            Grade: 2
          },
          {
            Description: "Answer 3",
            Grade: 3
          },
        ]
      }
    ]
  }

  let mockApiCallsService: Partial<ApiCallsService> = {
    getPersonalityTest() {
      return of(personalityTest);
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PersonalityTestComponent],
      providers: [
        { provide: ApiCallsService, useValue: mockApiCallsService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalityTestComponent);
    component = fixture.componentInstance;
    apiCallsService = TestBed.inject(ApiCallsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should create ItemsService", () => {
    expect(apiCallsService).toBeTruthy();
  });

  it("should get 1 personality test with 2 questions", () => {
    component.initializeTest();

    expect(component.personalityTest.Questions.length).toEqual(2);
  });

  it("should call the getPersonalityTest method from ApiCallsService", () => {
    let spyRef = spyOn(apiCallsService, "getPersonalityTest").and.callThrough();

    component.initializeTest();

    fixture.detectChanges();

    expect(spyRef).toHaveBeenCalled();
  });
});
