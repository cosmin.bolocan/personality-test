import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

//External imports
import { FlexLayoutModule } from '@angular/flex-layout';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';

//Material imports
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatStepperModule } from '@angular/material/stepper';
import { MatProgressBarModule } from '@angular/material/progress-bar';

//Local imports
import { PersonalityTestModule } from './modules/personality-test/personality-test.module';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { HeaderComponent } from './components/header/header.component';
import { ApiCallsService } from './services/ApiCalls.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChartModule } from 'primeng/chart';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LandingPageComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,

    //Material imports
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatStepperModule,
    MatProgressBarModule,

    //External imports
    FlexLayoutModule,
    ToastModule,

    PersonalityTestModule,
  ],
  providers: [
    ApiCallsService,
    MessageService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
