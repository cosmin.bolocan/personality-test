import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PersonalityTest } from '../models/PersonalityTest';

@Injectable({
  providedIn: 'root'
})
export class ApiCallsService {
  svcBaseUrl: string;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };
  personalityTest: PersonalityTest;

  constructor(
    public httpClient: HttpClient
  ) {
    this.svcBaseUrl = environment.urlAPI;
  }

  getPersonalityTest() {
    const callUrl = this.svcBaseUrl + "PersonalityTest";
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.httpClient.get(callUrl, {headers:headers});
  }


  setThePersonalityTestDone(personalityTest: PersonalityTest){
    this.personalityTest = personalityTest;
  }

  getThePersonalityTestDone(){
    return this.personalityTest;
  }

}
