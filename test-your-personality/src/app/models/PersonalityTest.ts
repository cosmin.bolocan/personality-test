export class PersonalityTest {
  public Questions: Question[];
}

export class Question {
  public Description: string;
  public Answers: Answer[];
  public Selected?: number;
}

export class Answer {
  public Description: string;
  public Grade: number;
}
