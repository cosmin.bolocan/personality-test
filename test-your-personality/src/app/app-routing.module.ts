import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './components/landing-page/landing-page.component';

const routes: Routes = [
  {
    path: "main",
    component: LandingPageComponent
  },
  // {
  //   path: "signin",
  //   component: SignInComponent//,
  //   //canActivate: [LoginGuard]
  // },
  {
    path: 'personality-test',
    loadChildren: () => import('./modules/personality-test/personality-test.module').then(m => m.PersonalityTestModule),
  },/* Lazy loading */
  {
    path: "**",
    redirectTo: "main"
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
