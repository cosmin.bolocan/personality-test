import { Component } from '@angular/core';
import { ApiCallsService } from './services/ApiCalls.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test-your-personality';

  constructor( ) { }
  
  ngOnInit() {
    
  }
}
