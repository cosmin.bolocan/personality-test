﻿using System.Text.Json.Serialization;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Linq;
using System;
using Microsoft.AspNetCore.Cors;

namespace Controllers
{
    [EnableCors("_myAllowSpecificOrigins")]
    [ApiController]
    [Route("[controller]")]
    public class PersonalityTest : ControllerBase
    {
        private readonly IPersonalityTestInterface _personalityTestProvider;
        public PersonalityTest([FromServices] IPersonalityTestInterface personalityTestProvider)
        {
            _personalityTestProvider = personalityTestProvider;
        }

        [HttpGet]
        public string Get()
        {
            return _personalityTestProvider.GetPersonalityTest();
        }
    }

}
