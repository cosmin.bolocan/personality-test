using System.Collections.Generic;
using System;


public class PersonalityTest
{
    public List<Question> Questions { get; set; }
}

public class Question
{
    public string Description { get; set; }
    public List<Answer> Answers { set; get; }
}

public class Answer
{
    public string Description { set; get; }
    public int Grade { get; set; }
}
