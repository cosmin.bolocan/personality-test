﻿using System.Collections.Generic;
using System.Text.Json;


public class PersonalityTestProvider : IPersonalityTestInterface
{
    public string GetPersonalityTest()
    {
        PersonalityTest personalityTest = new PersonalityTest
        {
            Questions = new List<Question>
            {
                new Question {
                    Description = "You’re really busy at work and a colleague is telling you their life story and personal woes. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Don’t dare to interrupt them", Grade = 1 },
                        new Answer { Description = "Think it’s more important to give them some of your time; work can wait", Grade = 2 },
                        new Answer { Description = "Listen, but with only with half an ear", Grade = 3 },
                        new Answer { Description = "Interrupt and explain that you are really busy at the moment", Grade = 4 }
                    }
                },
                new Question {
                    Description = "You’ve been sitting in the doctor’s waiting room for more than 25 minutes. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Look at your watch every two minutes", Grade = 1 },
                        new Answer { Description = "Bubble with inner anger, but keep quiet", Grade = 2 },
                        new Answer { Description = "Explain to other equally impatient people in the room that the doctor is always running late", Grade = 3 },
                        new Answer { Description = "Complain in a loud voice, while tapping your foot impatiently", Grade = 4 }
                    }
                },
                new Question {
                    Description = "You’re having an animated discussion with a colleague regarding a project that you’re in charge of. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Don’t dare contradict them", Grade = 1 },
                        new Answer { Description = "Think that they are obviously right", Grade = 2 },
                        new Answer { Description = "Defend your own point of view, tooth and nail", Grade = 3 },
                        new Answer { Description = "Continuously interrupt your colleague", Grade = 4 }
                    }
                },
                new Question {
                    Description = "You are taking part in a guided tour of a museum. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Are a bit too far towards the back so don’t really hear what the guide is saying", Grade = 1 },
                        new Answer { Description = "Follow the group without question", Grade = 2 },
                        new Answer { Description = "Make sure that everyone is able to hear properly", Grade = 3 },
                        new Answer { Description = "Are right up the front, adding your own comments in a loud voice", Grade = 4 }
                    }
                },
                new Question {
                    Description = "During dinner parties at your home, you have a hard time with people who:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Ask you to tell a story in front of everyone else", Grade = 1 },
                        new Answer { Description = "Talk privately between themselves", Grade = 2 },
                        new Answer { Description = "Hang around you all evening", Grade = 3 },
                        new Answer { Description = "Always drag the conversation back to themselves", Grade = 4 }
                    }
                },
                new Question {
                    Description = "You crack a joke at work, but nobody seems to have noticed. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Think it’s for the best — it was a lame joke anyway", Grade = 1 },
                        new Answer { Description = "Wait to share it with your friends after work", Grade = 2 },
                        new Answer { Description = "Try again a bit later with one of your colleagues", Grade = 3 },
                        new Answer { Description = "Keep telling it until they pay attention", Grade = 4 }
                    }
                },
                new Question {
                    Description = "This morning, your agenda seems to be free. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Know that somebody will find a reason to come and bother you", Grade = 1 },
                        new Answer { Description = "Heave a sigh of relief and look forward to a day without stress", Grade = 2 },
                        new Answer { Description = "Question your colleagues about a project that’s been worrying you", Grade = 3 },
                        new Answer { Description = "Pick up the phone and start filling up your agenda with meetings", Grade = 4 }
                    }
                },
                new Question {
                    Description = "During dinner, the discussion moves to a subject about which you know nothing at all. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Don’t dare show that you don’t know anything about the subject", Grade = 1 },
                        new Answer { Description = "Barely follow the discussion", Grade = 2 },
                        new Answer { Description = "Ask lots of questions to learn more about it", Grade = 3 },
                        new Answer { Description = "Change the subject of discussion", Grade = 4 }
                    }
                },
                new Question {
                    Description = "You’re out with a group of friends and there’s a person who’s quite shy and doesn’t talk much. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Notice that they’re alone, but don’t go over to talk with them", Grade = 1 },
                        new Answer { Description = "Go and have a chat with them", Grade = 2 },
                        new Answer { Description = "Shoot some friendly smiles in their direction", Grade = 3 },
                        new Answer { Description = "Hardly notice them at all", Grade = 4 }
                    }
                },
                new Question {
                    Description = "At work, somebody asks for your help for the hundredth time. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Give them a hand, as usual", Grade = 1 },
                        new Answer { Description = "Accept — you’re known for being helpful", Grade = 2 },
                        new Answer { Description = "Ask them, please, to find somebody else for a change", Grade = 3 },
                        new Answer { Description = "Loudly make it known that you’re annoyed", Grade = 4 }
                    }
                },
                new Question {
                    Description = "You’ve been see a movie with your family and the reviews are mixed. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Don’t share your point of view with anyone", Grade = 1 },
                        new Answer { Description = "Didn’t like the film, but keep your views to yourself when asked", Grade = 2 },
                        new Answer { Description = "State your point of view with enthusiasm", Grade = 3 },
                        new Answer { Description = "Try to bring the others round to your point of view", Grade = 4 }
                    }
                },
                new Question {
                    Description = "A friend arrives late for your meeting. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Say, ‘It’s not a problem,’ even if that’s not what you really think", Grade = 1 },
                        new Answer { Description = "Give them a filthy look and sulk for the rest of the evening", Grade = 2 },
                        new Answer { Description = "Tell them, ‘You’re too much! Have you seen the time?’", Grade = 3 },
                        new Answer { Description = "Make a scene in front of everyone", Grade = 4 }
                    }
                },
                new Question {
                    Description = "You can’t find your car keys. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Don’t want anyone to find out, so you take the bus instead", Grade = 1 },
                        new Answer { Description = "Panic and search madly without asking anyone for help", Grade = 2 },
                        new Answer { Description = "Grumble without telling your family why you’re in a bad mood", Grade = 3 },
                        new Answer { Description = "Accuse those around you for misplacing them", Grade = 4 }
                    }
                },
                new Question {
                    Description = "It’s time for your annual appraisal with your boss. You:",
                    Answers = new List<Answer>
                    {
                        new Answer { Description = "Go with great hesitation as these sessions are torture for you", Grade = 1 },
                        new Answer { Description = "Look forward to hearing what your boss thinks about you and expects from you", Grade = 2 },
                        new Answer { Description = "Rehearse ad nauseam the arguments and ideas that you’ve prepared for the meeting", Grade = 3 },
                        new Answer { Description = "Go along unprepared as you are confident and like improvising", Grade = 4 }
                    }
                }
            }
        };

        return JsonSerializer.Serialize<global::PersonalityTest>(personalityTest);
    }
}

